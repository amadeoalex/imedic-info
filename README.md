# DISCLAIMER
This app was created, rather "committed", relatively long time ago. While some of the project is written properly and can be of use for someone, it is full of bad practices and now it serves no other purpose rather than show that I am actually making progress ;).
Please **DO NOT** follow the deign patterns used in this project

# iMedic Info

This ~~is~~ was an informational Android application of iMEDIC International Medical Interdisciplinary Congress held annually by the Collegium Medicum in Bydgoszcz - Poland.
Within the app users can find up to date informations about the congress itself, contacts to most important organizers, rules and the interactive map of the building where the conference takes place.

## Project status

 - [x] Dead
 - [x] Total rewrite completed


## Installation

You cannot download the app anymore (except maybe websites like apkmirror).

## Built With

* [Android Studio](https://developer.android.com/studio/)
* [Gradle](https://gradle.org/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Note

During the development at the beginning my aim was to create the the app according to "highest" standards that were possible to reach by me. However as time went by I had less and less time to sort and smooth everything out and so, some parts of the app are basically a technological debt. I have the time to pay it until the next conference so please, as long as this note exists do not treat this code as one that even lied, near the production code.
