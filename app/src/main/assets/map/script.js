var map = document.getElementById("blat")

map.addEventListener("click", function(e) {
    var target = e.target

    if (target.nodeName === "rect" || target.nodeName === "path" || target.nodeName === "polygon") {
        if(typeof jsaProxy !== "undefined"){
            jsaProxy.showInformation(target.id)
        }
    }
});