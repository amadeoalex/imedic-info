package amadeo.imedicinfo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-02-04.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class WelcomeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
    private final String TAG = "iMedic WA";
    private final boolean FORCE_COPY = true;

    private ViewPager viewPager;
    private TabLayout dotContainer;
    private Button nextButton;
    private Button skipButton;

    private int[] slides;
    private int[] colors;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int ui_settings = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
        getWindow().getDecorView().setSystemUiVisibility(ui_settings);

        setContentView(R.layout.activity_welcome);

        viewPager = findViewById(R.id.welcomeViewPager);
        dotContainer = findViewById(R.id.welcomeDotContainer);
        nextButton = findViewById(R.id.welcomeNextButton);
        skipButton = findViewById(R.id.welcomeSkipButton);

        slides = new int[]{R.layout.welcome_screen1, R.layout.welcome_screen2, R.layout.welcome_screen3};
        colors = new int[]{R.color.medicalColor, R.color.pharmaceuticalColor, R.color.healthColor};

        PagerAdapter pagerAdapter = new WelcomePagerAdapter();
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);

        dotContainer.setupWithViewPager(viewPager, true);

        skipButton.setOnClickListener(listener -> finished());

        nextButton.setOnClickListener(listener -> {
            int currentSlide = viewPager.getCurrentItem();
            if (currentSlide == slides.length - 1)
                finished();
            else
                viewPager.setCurrentItem(currentSlide + 1);
        });

        if (Utils.firstRun(getApplicationContext())) {
            Utils.logI(TAG, "first time launch preparations");

            new Thread(() -> {
                checkAssets();
                sendBroadcast(new Intent("amadeo.imedic.action.UPDATE"));
            }).start();
        }
    }

    private void finished() {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putBoolean("APP_AFTER_FIRST_RUN", true).apply();
        sendBroadcast(new Intent("amadeo.imedic.action.SET_ALARM"));
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == slides.length - 1) {
            nextButton.setText(getResources().getString(R.string.finish));
        } else {
            nextButton.setText(getResources().getString(R.string.next));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private class WelcomePagerAdapter extends PagerAdapter {

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(slides[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return slides.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }


    }

    private void checkAssets() {
        copyToInternalStorage("organizers.conf", FORCE_COPY);
        copyToInternalStorage("info", FORCE_COPY);
        copyToInternalStorage("map", FORCE_COPY);
        copyToInternalStorage("rules.conf", FORCE_COPY);
    }

    private void copyToInternalStorage(String name, boolean force) {
        try {
            String contents[] = getAssets().list(name);
            if (contents.length == 0) {
                File file = new File(getApplicationContext().getFilesDir().getAbsolutePath() + "/" + name);
                if (file.exists() && !force)
                    return;

                Utils.logI(TAG, "copying %s", file.getAbsolutePath());

                InputStream inputStream = getAssets().open(name);

                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                    Utils.logI("imedic", "creating dir");
                }

                OutputStream outputStream = new FileOutputStream(file);

                byte[] buffer = new byte[1024];
                int read;
                while ((read = inputStream.read(buffer)) != -1)
                    outputStream.write(buffer, 0, read);

                inputStream.close();
                outputStream.close();
            } else {
                for (String item : contents) {
                    copyToInternalStorage(name + "/" + item, FORCE_COPY);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
