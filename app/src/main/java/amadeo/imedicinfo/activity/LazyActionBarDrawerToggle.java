package amadeo.imedicinfo.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.lang.ref.WeakReference;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;
import amadeo.imedicinfo.activity.information.AgendaActivity;
import amadeo.imedicinfo.drawer.ComingSoonFragment;
import amadeo.imedicinfo.drawer.agenda.CaseSessionFragment;
import amadeo.imedicinfo.drawer.agenda.HealthSessionFragment;
import amadeo.imedicinfo.drawer.agenda.MedicalSessionFragment;
import amadeo.imedicinfo.drawer.agenda.PharmaceuticalSessionFragment;
import amadeo.imedicinfo.drawer.agenda.PosterSessionFragment;
import amadeo.imedicinfo.drawer.debug.DebugFragment;
import amadeo.imedicinfo.drawer.main.MainFragment;
import amadeo.imedicinfo.drawer.map.MapFragment;
import amadeo.imedicinfo.drawer.organizers.OrganizersFragment;
import amadeo.imedicinfo.drawer.rules.RulesFragment;
import amadeo.imedicinfo.drawer.settings.SettingsFragment;

/**
 * Created by Amadeo on 2018-02-18.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

class LazyActionBarDrawerToggle extends ActionBarDrawerToggle implements NavigationView.OnNavigationItemSelectedListener {
    private final String TAG = "iMedic DrawerToggle";

    private final DrawerLayout drawerLayout;
    private final WeakReference<MainActivity> weakActivity;
    private int selectedItemId;
    private int previousSelectedItemId = R.id.nav_main;

    public LazyActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar) {
        super(activity, drawerLayout, toolbar, R.string.navigation_drawer_close, R.string.navigation_drawer_close);

        this.drawerLayout = drawerLayout;
        this.weakActivity = new WeakReference<>((MainActivity) activity);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);

        selectedItemId = item.getItemId();

        if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
            Fragment fragment = null;
            MainActivity activity = weakActivity.get();

            switch (item.getItemId()) {
                case R.id.nav_main:
                    fragment = new MainFragment();
                    activity.setActionBarTitle(R.string.main);
                    break;
                case R.id.nav_rules:
                    fragment = new RulesFragment();
                    activity.setActionBarTitle(R.string.rules);
                    break;
                case R.id.nav_register:
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(activity.getString(R.string.wwwRegister))));
                    break;
                case R.id.nav_map:
                    fragment = new MapFragment();
                    activity.setActionBarTitle(R.string.map);
                    break;
                case R.id.nav_agenda:
                    activity.startActivity(new Intent(activity.getApplicationContext(), AgendaActivity.class));
                    break;
                case R.id.nav_session_medical:
                    if (MedicalSessionFragment.isAvailable(weakActivity.get()))
                        fragment = new MedicalSessionFragment();
                    else
                        fragment = new ComingSoonFragment();
                    activity.setActionBarTitle(R.string.medical);
                    break;
                case R.id.nav_session_pharmaceutical:
                    if (PharmaceuticalSessionFragment.isAvailable(weakActivity.get()))
                        fragment = new PharmaceuticalSessionFragment();
                    else
                        fragment = new ComingSoonFragment();
                    activity.setActionBarTitle(R.string.pharmaceutical);
                    break;
                case R.id.nav_session_health:
                    if (HealthSessionFragment.isAvailable(weakActivity.get()))
                        fragment = new HealthSessionFragment();
                    else
                        fragment = new ComingSoonFragment();
                    activity.setActionBarTitle(R.string.health);
                    break;
                case R.id.nav_session_case:
                    if (CaseSessionFragment.isAvailable(weakActivity.get()))
                        fragment = new CaseSessionFragment();
                    else
                        fragment = new ComingSoonFragment();
                    activity.setActionBarTitle(R.string.caseSession);
                    break;
                case R.id.nav_session_poster:
                    if (PosterSessionFragment.isAvailable(weakActivity.get()))
                        fragment = new PosterSessionFragment();
                    else
                        fragment = new ComingSoonFragment();
                    activity.setActionBarTitle(R.string.posterSession);
                    break;
                case R.id.nav_organisers:
                    fragment = new OrganizersFragment();
                    activity.setActionBarTitle(R.string.organizers);
                    break;
                case R.id.nav_settings:
                    fragment = new SettingsFragment();
                    activity.setActionBarTitle(R.string.settings);
                    break;
                case R.id.nav_debug:
                    fragment = new DebugFragment();
                    activity.setActionBarTitle(R.string.debug);
                    break;
            }

            if (fragment != null) {
                previousSelectedItemId = selectedItemId;

                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                Fragment currentFragment = fragmentManager.findFragmentByTag(fragment.getClass().getName());

                if (currentFragment == null) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.fragment_enter, R.anim.framgment_exit);

                    fragmentTransaction.replace(R.id.content_main, fragment, fragment.getClass().getName());
                    fragmentTransaction.commit();
                } else {
                    Utils.logI(TAG, "fragment already visible");
                }
            } else {
                Utils.logI(TAG, "fragment is null - link/activity?");
                NavigationView navigationView = drawerLayout.findViewById(R.id.nav_view);
                navigationView.setCheckedItem(previousSelectedItemId);
            }
        }

        return true;
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (selectedItemId > 0) {
            if (drawerView instanceof NavigationView) {
                NavigationView navigationView = (NavigationView) drawerView;
                navigationView.getMenu().performIdentifierAction(selectedItemId, 0);

                selectedItemId = -1;
            }
        }
    }
}
