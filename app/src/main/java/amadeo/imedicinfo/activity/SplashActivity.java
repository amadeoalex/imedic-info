package amadeo.imedicinfo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-02-03.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.firstRun(getApplicationContext()))
            startActivity(new Intent(this, WelcomeActivity.class));
        else {
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}
