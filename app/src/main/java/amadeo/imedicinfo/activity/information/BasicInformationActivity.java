package amadeo.imedicinfo.activity.information;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

public class BasicInformationActivity extends AppCompatActivity {

    public BasicInformationActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_information);

        Toolbar toolbar = findViewById(R.id.basic_info_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.basic_info_collapsingToolbar);

        ImageView backgroundImage = findViewById(R.id.basic_info_toolbarImage);
        TextView content = findViewById(R.id.basic_info_content);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String contentPath = "info/" + extras.getString("id") + "/";

            JSONObject info;
            try {
                info = new JSONObject(extras.getString("json"));

                collapsingToolbar.setTitle(info.optString("title"));

                Bitmap background = BitmapFactory.decodeFile(Utils.getInternalFilePath(this, contentPath + "data/background.jpg"));
                backgroundImage.setImageBitmap(background);

                if (info.getBoolean("html")) {
                    String contentString = Utils.loadInternalResourceAsString(this, contentPath + "data/content.html");
                    content.setText(Html.fromHtml(contentString));
                } else {
                    String contentString = Utils.loadInternalResourceAsString(this, contentPath + "data/content.txt");
                    content.setText(contentString);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
