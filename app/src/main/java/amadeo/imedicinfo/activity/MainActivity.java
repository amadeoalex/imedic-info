package amadeo.imedicinfo.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import amadeo.imedicinfo.NotificationBroadcastListener;
import amadeo.imedicinfo.R;
import amadeo.imedicinfo.UpdateAsyncTask;
import amadeo.imedicinfo.Utils;
import amadeo.imedicinfo.drawer.main.MainFragment;

public class MainActivity extends AppCompatActivity implements UpdateAsyncTask.UpdateCallback {

    private final String TAG = "iMedic MA";
    private View rootView;
    private LazyActionBarDrawerToggle toggle;
    private NavigationView navigationView;

    @Override
    public void showSnackBar(String message) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        rootView = findViewById(R.id.root_layout);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggle = new LazyActionBarDrawerToggle(this, drawer, toolbar);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(toggle);

        if (savedInstanceState != null) {
            String fragmentName = savedInstanceState.getString("active_fragment");
            Utils.logI(TAG, "retrieved fragment: %s", fragmentName);

            try {
                Class<?> fragmentClass = Class.forName(fragmentName);
                Object fragment = fragmentClass.newInstance();

                getSupportFragmentManager().beginTransaction().replace(R.id.content_main, (Fragment) fragment, fragmentName).commit();
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }

        } else {
            Fragment startFragment = new MainFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, startFragment, startFragment.getClass().getName()).commit();
        }

        //Just to be 100% sure
        Intent launchIntent = new Intent(getApplicationContext(), NotificationBroadcastListener.class);
        launchIntent.setAction("amadeo.imedic.action.NOTIFICATION");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, launchIntent, PendingIntent.FLAG_NO_CREATE);

        if (alarmIntent == null) {
            Utils.logI(TAG, "'emergency' alarm set?");
            sendBroadcast(new Intent("amadeo.imedic.action.SET_ALARM"));
        }

        sendBroadcast(new Intent("amadeo.imedic.action.UPDATE"));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Utils.logI(TAG, "received new intent");

        toggle.onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_main));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content_main);
        String fragmentName = currentFragment.getClass().getName();
        Utils.logI(TAG, "stored fragment: %s", fragmentName);

        outState.putString("active_fragment", fragmentName);
    }

    public void setActionBarTitle(int stringId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(stringId);
    }
}
