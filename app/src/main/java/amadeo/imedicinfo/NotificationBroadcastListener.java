package amadeo.imedicinfo;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

import amadeo.imedicinfo.activity.MainActivity;

/**
 * Created by Amadeo on 2018-02-22.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class NotificationBroadcastListener extends BroadcastReceiver {

    private final String TAG = "iMedic Notification";
    public static final String CHANNEL_ID = "iMedic Notification";
    private final String PREF_KEY = "pref_key_allow_notifications";

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction() != null && intent.getAction().equals("amadeo.imedic.action.NOTIFICATION")) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            if (sharedPreferences.getBoolean(PREF_KEY, true)) {
                Utils.logI(TAG, "notification check");

                try {
                    String jsonString = Utils.loadInternalResourceAsString(context, "info/notificationSchedule.json");
                    JSONObject notificationSchedule = new JSONObject(jsonString);

                    @SuppressLint("SimpleDateFormat") String currentDate = new SimpleDateFormat("dd.MM.yyyy").format(System.currentTimeMillis());
                    Utils.logI(TAG, "checking notification for %s", currentDate);

                    if (notificationSchedule.has(currentDate)) {
                        Utils.logI(TAG, "notification found");

                        JSONObject scheduledNotification = notificationSchedule.getJSONObject(currentDate);

                        Intent touchIntent = new Intent(context, MainActivity.class);
                        intent.setAction(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("notification", "");

                        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, touchIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_round))
                                .setSmallIcon(R.drawable.ic_notification)
                                .setColor(ContextCompat.getColor(context, R.color.iMedicColor))
                                .setContentTitle(scheduledNotification.optString("title", "ERROR"))
                                .setContentText(scheduledNotification.optString("content", "ERROR"))
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true);

                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                        notificationManager.notify(0, mBuilder.build());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.logI(TAG, "notification check - disabled via settings");
            }

        }
    }

}
