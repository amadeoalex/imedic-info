package amadeo.imedicinfo;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Amadeo on 2018-02-03.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class Utils {
    private final static boolean LOG_ENABLED = false;

    public static void logI(String tag, String message, Object... params) {
        if (LOG_ENABLED) {
            String formattedString = String.format(message, params);
            Log.i(tag, formattedString);
        }
    }

    public static void logW(String tag, String message, Object... params) {
        if (LOG_ENABLED) {
            String formattedString = String.format(message, params);
            Log.w(tag, formattedString);
        }
    }

    public static String loadResourceAsString(Context context, String path) {
        InputStream inputStream = null;
        Scanner scanner = null;
        try {
            inputStream = context.getAssets().open(path);
            scanner = new Scanner(inputStream).useDelimiter("\\A");

            return scanner.hasNext() ? scanner.next() : "";
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();

                if (scanner != null)
                    scanner.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static String loadInternalResourceAsString(Context context, String path) {
        File file = new File(context.getFilesDir().getAbsolutePath() + "/" + path);
        Scanner scanner;
        try {
            scanner = new Scanner(file).useDelimiter("\\A");

            return scanner.hasNext() ? scanner.next() : "";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getInternalFilePath(Context context, String path) {
        return context.getFilesDir() + "/" + path;
    }

    public static String getURLResourceAsString(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

//            StringBuilder stringBuilder = new StringBuilder();
//            String line;
//            while ((line = bufferedReader.readLine()) != null) {
//                stringBuilder.append(line);
//            }
//
//            return stringBuilder.toString();

            Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
            String result = scanner.hasNext() ? scanner.next() : "";

            scanner.close();

            return result;
        } catch (ConnectException e) {
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject getURLResourceAsJSON(String urlString) {
        try {
            String jsonString = getURLResourceAsString(urlString);
            if (jsonString != null)
                return new JSONObject(getURLResourceAsString(urlString));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean firstRun(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return !sharedPreferences.contains("APP_AFTER_FIRST_RUN");
    }
}
