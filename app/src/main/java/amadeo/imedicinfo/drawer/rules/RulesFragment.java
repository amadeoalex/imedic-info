package amadeo.imedicinfo.drawer.rules;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-02-13.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class RulesFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rules, container, false);

        TextView rules = view.findViewById(R.id.rulesTextView2);

        String rulesText = Utils.loadInternalResourceAsString(requireActivity().getApplicationContext(), "rules.conf");

        if (rulesText != null) {
            rulesText = rulesText.replace("\n", "<br>").replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
            //String rulesText = getActivity().getString(R.string.rules_1);
            rules.setText(Html.fromHtml(rulesText));
        }

        return view;
    }
}
