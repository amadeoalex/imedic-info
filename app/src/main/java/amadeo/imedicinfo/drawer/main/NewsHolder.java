package amadeo.imedicinfo.drawer.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import amadeo.imedicinfo.R;

/**
 * Created by Amadeo on 2018-03-17.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class NewsHolder extends RecyclerView.ViewHolder {
    private final TextView title;
    private final TextView content;
    private final TextView date;

    public NewsHolder(View itemView) {
        super(itemView);

        title = itemView.findViewById(R.id.newsTitle);
        content = itemView.findViewById(R.id.newsContent);
        date = itemView.findViewById(R.id.newsDate);
    }

    public void bindNews(JSONObject news) {
        title.setText(news.optString("title"));
        content.setText(news.optString("content"));
        date.setText(news.optString("date"));
    }
}
