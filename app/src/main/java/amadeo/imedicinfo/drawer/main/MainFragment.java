package amadeo.imedicinfo.drawer.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-02-04.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class MainFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.mainRecyclerView);

        try {
            JSONObject notificationSchedule = new JSONObject(Utils.loadInternalResourceAsString(requireActivity(), "info/notificationSchedule.json"));
            recyclerView.setAdapter(new NewsAdapter(notificationSchedule));

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
