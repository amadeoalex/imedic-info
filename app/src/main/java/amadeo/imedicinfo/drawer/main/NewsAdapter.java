package amadeo.imedicinfo.drawer.main;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-03-17.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

class NewsAdapter extends RecyclerView.Adapter<NewsHolder> {
    private final JSONArray news;

    public NewsAdapter(JSONObject notificationSchedule) {
        long now = System.currentTimeMillis();

        ArrayList<JSONObject> unsortedNews = new ArrayList<>();

        Iterator<String> keys = notificationSchedule.keys();
        while (keys.hasNext()) {
            try {
                String dateString = keys.next();

                @SuppressLint("SimpleDateFormat") long notificationMillis = new SimpleDateFormat("dd.MM.yyyy").parse(dateString).getTime();
                if (notificationMillis <= now) {
                    Utils.logI("iMedic NEWS", "adding %s", dateString);

                    JSONObject notification = notificationSchedule.getJSONObject(dateString);
                    notification.put("date", dateString);

                    unsortedNews.add(notification);
                }

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(unsortedNews, Collections.reverseOrder((jsonObject1, jsonObject2) -> jsonObject1.optString("date").compareTo(jsonObject2.optString("date"))));
        news = new JSONArray(unsortedNews);
    }

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_main_row, parent, false);

        return new NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHolder holder, int position) {
        holder.bindNews(news.optJSONObject(position));
    }

    @Override
    public int getItemCount() {
        return news.length();
    }
}
