package amadeo.imedicinfo.drawer.debug;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import amadeo.imedicinfo.NotificationBroadcastListener;
import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;
import amadeo.imedicinfo.UpdateAsyncTask;

/**
 * Created by Amadeo on 2018-02-22.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class DebugFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, container, false);

        view.findViewById(R.id.buttonNotif).setOnClickListener(listener -> {
            AlarmManager alarmManager = (AlarmManager) requireActivity().getSystemService(Context.ALARM_SERVICE);

            Intent launchIntent = new Intent(requireActivity().getApplicationContext(), NotificationBroadcastListener.class);
            launchIntent.setAction("amadeo.imedic.action.NOTIFICATION");
            PendingIntent alarmIntent = PendingIntent.getBroadcast(requireActivity().getApplicationContext(), 0, launchIntent, PendingIntent.FLAG_NO_CREATE);

            if (alarmIntent == null)
                Toast.makeText(getActivity(), "alarm is not working", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getActivity(), "alarm is working", Toast.LENGTH_SHORT).show();

        });

        view.findViewById(R.id.buttonDebug2).setOnClickListener(listener -> requireActivity().sendBroadcast(new Intent("amadeo.imedic.action.SET_ALARM")));

        view.findViewById(R.id.buttonDebug3).setOnClickListener(listener -> requireActivity().sendBroadcast(new Intent("amadeo.imedic.action.REMOVE_ALARM")));

        view.findViewById(R.id.buttonDebug4).setOnClickListener(listener -> requireActivity().sendBroadcast(new Intent("amadeo.imedic.action.NOTIFICATION")));

        view.findViewById(R.id.debugResetFirst).setOnClickListener(listener -> {
            Utils.logI("iMedic", "?");
            PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().remove("APP_AFTER_FIRST_RUN").apply();
            Toast.makeText(getActivity(), Boolean.toString(Utils.firstRun(getActivity())), Toast.LENGTH_SHORT).show();
        });

        view.findViewById(R.id.buttonDebugUpdate).setOnClickListener(listener -> {
            UpdateAsyncTask updateAsyncTask = new UpdateAsyncTask(requireActivity().getApplicationContext());
            updateAsyncTask.execute();
        });

        view.findViewById(R.id.buttonDebugResetVersion).setOnClickListener(listener -> {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(requireActivity().getApplicationContext());
            SharedPreferences.Editor editor = preferences.edit();

            editor.putFloat("APP_DATA_VERSION", 0f);
            editor.putFloat("APP_ORGANIZERS_VERSION", 0f);
            editor.putFloat("APP_MAP_VERSION", 0f);
            editor.putFloat("APP_INFO_VERSION", 0f);
            editor.putFloat("APP_RULES_VERSION", 0f);

            editor.apply();
        });

        view.findViewById(R.id.buttonDebugResetDelay).setOnClickListener(listener -> {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(requireActivity().getApplicationContext());
            SharedPreferences.Editor editor = preferences.edit();

            editor.putLong("APP_LAST_UPDATE_MILLIS", 0);
            editor.apply();
        });

        return view;
    }

}
