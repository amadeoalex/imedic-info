package amadeo.imedicinfo.drawer.agenda;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-05-25.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class PosterSessionFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_session, container, false);

        ImageView background = view.findViewById(R.id.sessionBackground);
        RecyclerView recyclerView = view.findViewById(R.id.sessionRecyclerView);

        DrawableCompat.setTint(background.getDrawable(), ContextCompat.getColor(getContext(), R.color.colorAccent));


        String jsonString = Utils.loadInternalResourceAsString(requireActivity(), "info/agenda/poster.json");
        try {
            JSONArray timeTable = new JSONArray(jsonString);

            PresentationAdapter adapter = new PresentationAdapter(timeTable, PresentationAdapter.MEDICAL);
            recyclerView.setAdapter(adapter);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public static boolean isAvailable(Context context) {
        File file = new File(Utils.getInternalFilePath(context, "info/agenda/poster.json"));
        return file.exists() && !file.isDirectory();
    }
}
