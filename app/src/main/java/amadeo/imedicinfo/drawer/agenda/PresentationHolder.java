package amadeo.imedicinfo.drawer.agenda;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import amadeo.imedicinfo.R;

import static amadeo.imedicinfo.drawer.agenda.PresentationAdapter.HEALTH;
import static amadeo.imedicinfo.drawer.agenda.PresentationAdapter.MEDICAL;
import static amadeo.imedicinfo.drawer.agenda.PresentationAdapter.PHARMACEUTICAL;

/**
 * Created by Amadeo on 2018-02-21.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class PresentationHolder extends RecyclerView.ViewHolder {
    private final TextView hour;
    private final TextView title;
    private final TextView owner;
    private final View spacer;

    private final String type;
    private JSONObject jsonObject;

    public PresentationHolder(View itemView, String type) {
        super(itemView);

        hour = itemView.findViewById(R.id.presentationHour);
        title = itemView.findViewById(R.id.presentationTitle);
        owner = itemView.findViewById(R.id.presentationOwner);
        spacer = itemView.findViewById(R.id.presentationSpacer);

        this.type = type;

        itemView.setOnClickListener(listener -> {
            if (jsonObject.has("abstract")) {
                LayoutInflater inflater = LayoutInflater.from(itemView.getContext());
                View view = inflater.inflate(R.layout.dialog_abstract, null);
                TextView abstractContent = view.findViewById(R.id.abstractContent);
                abstractContent.setText(jsonObject.optString("abstract"));

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(itemView.getContext());
                alertDialogBuilder.setTitle(jsonObject.optString("event"));
                alertDialogBuilder.setView(view);
                alertDialogBuilder.setNeutralButton(android.R.string.ok, null);

                alertDialogBuilder.show();
            }
        });
    }

    public void bindOrganizer(JSONObject jsonObject) {
        this.jsonObject = jsonObject;

        try {
            hour.setText(jsonObject.getString("time"));
            title.setText(jsonObject.getString("event"));

            String ownerString = jsonObject.optString("owner", null);
            if (ownerString != null) {
                owner.setText(ownerString);
            } else
                owner.setVisibility(View.GONE);

            switch (type) {
                case MEDICAL:
                    spacer.setBackgroundColor(ContextCompat.getColor(spacer.getContext(), R.color.medicalColor));
                    break;
                case PHARMACEUTICAL:
                    spacer.setBackgroundColor(ContextCompat.getColor(spacer.getContext(), R.color.pharmaceuticalColor));
                    break;
                case HEALTH:
                    spacer.setBackgroundColor(ContextCompat.getColor(spacer.getContext(), R.color.healthColor));
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
