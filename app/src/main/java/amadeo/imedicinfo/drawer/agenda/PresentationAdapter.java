package amadeo.imedicinfo.drawer.agenda;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import amadeo.imedicinfo.R;

/**
 * Created by Amadeo on 2018-02-21.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

class PresentationAdapter extends RecyclerView.Adapter<PresentationHolder> {
    public static final String MEDICAL = "medical.json";
    public static final String PHARMACEUTICAL = "pharmaceutical.json";
    public static final String HEALTH = "health.json";

    private final JSONArray session;
    private final String type;

    public PresentationAdapter(JSONArray session, String type) {
        this.session = session;
        this.type = type;
    }

    @NonNull
    @Override
    public PresentationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_session_card, parent, false);

        return new PresentationHolder(view, type);

    }

    @Override
    public void onBindViewHolder(@NonNull PresentationHolder holder, int position) {
        try {
            holder.bindOrganizer(session.getJSONObject(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return session.length();
    }
}
