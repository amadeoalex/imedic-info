package amadeo.imedicinfo.drawer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import amadeo.imedicinfo.R;

/**
 * Created by Amadeo on 2018-03-10.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class ComingSoonFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_soon, container, false);
    }
}
