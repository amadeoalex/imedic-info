package amadeo.imedicinfo.drawer.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.io.File;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-02-03.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class MapFragment extends Fragment {
    private static final String TAG = "iMedic Map";

    private WebView webView;
    private ProgressBar progressBar;

    private String lastMapPath = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        webView = view.findViewById(R.id.mapWebView);
        progressBar = view.findViewById(R.id.mapProgressBar);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);

        webView.addJavascriptInterface(new AppJavaScriptInterface(requireActivity(), webView), "jsaProxy");
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                Utils.logI(TAG, "page loaded %d", System.currentTimeMillis());
                progressBar.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.VISIBLE);
            }
        });


        setHasOptionsMenu(true);

        loadMap("map/groundFloor.html", R.string.groundFloor);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_map, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map_legend:
                AlertDialog alertDialog = new AlertDialog.Builder(requireActivity()).create();
                alertDialog.setTitle(getResources().getString(R.string.legend));
                alertDialog.setMessage(getResources().getString(R.string.legend_description));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        (dialog, which) -> dialog.dismiss());
                alertDialog.show();
                break;
            case R.id.action_map_ground: {
                loadMap("map/groundFloor.html", R.string.groundFloor);
                break;
            }
            case R.id.action_map_first: {
                loadMap("map/firstFloor.html", R.string.firstFloor);
                break;
            }
            case R.id.action_map_second: {
                loadMap("map/secondFloor.html", R.string.secondFloor);
                break;
            }
        }


        return super.onOptionsItemSelected(item);
    }

    private void loadMap(String mapPath, int title) {
        if (!lastMapPath.equals(mapPath)) {
            File map = new File(Utils.getInternalFilePath(requireActivity(), mapPath));
            Utils.logI(TAG, "loading %d", System.currentTimeMillis());
            webView.loadUrl("file:///" + map);

            ActionBar actionBar = ((AppCompatActivity) requireActivity()).getSupportActionBar();
            if (actionBar != null)
                actionBar.setTitle(title);

            lastMapPath = mapPath;
        }
    }

}
