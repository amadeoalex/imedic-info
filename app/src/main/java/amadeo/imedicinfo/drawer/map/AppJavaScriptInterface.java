package amadeo.imedicinfo.drawer.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import amadeo.imedicinfo.Utils;
import amadeo.imedicinfo.activity.information.AgendaActivity;
import amadeo.imedicinfo.activity.information.BasicInformationActivity;

/**
 * Created by Amadeo on 2018-02-03.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

class AppJavaScriptInterface {
    private static final String TAG = "iMedic JavaScript";
    private final Activity activity;

    public AppJavaScriptInterface(Activity activity, WebView webView) {
        this.activity = activity;
    }

    @JavascriptInterface
    public void showInformation(String id) {
        //Toast.makeText(activity, id, Toast.LENGTH_SHORT).show();

        try {
            if (id.endsWith("_a")) {
                id = id.replace("_x5F_a", "");

                //My God please forgive me....
                if(id.startsWith("lectureHall")){
                    Intent intent = new Intent(activity, AgendaActivity.class);
                    activity.startActivity(intent);

                    return;
                }

                File file = new File(activity.getFilesDir() + "/info/" + id);
                if (file.exists()) {
                    String jsonString = Utils.loadInternalResourceAsString(activity, "/info/" + id + "/info.json");
                    JSONObject info = new JSONObject(jsonString);
                    if (info.getString("type").equals("basic_info")) {
                        Intent intent = new Intent(activity, BasicInformationActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        bundle.putString("json", jsonString);
                        intent.putExtras(bundle);

                        activity.startActivity(intent);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
