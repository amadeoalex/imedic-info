package amadeo.imedicinfo.drawer.organizers;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;

import amadeo.imedicinfo.R;

/**
 * Created by Amadeo on 2018-02-13.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

class OrganizerAdapter extends RecyclerView.Adapter<OrganizerHolder> {

    private final JSONArray organizers;

    public OrganizerAdapter(JSONArray organizers) {
        this.organizers = organizers;
    }

    @NonNull
    @Override
    public OrganizerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_organizers_row, parent, false);

        return new OrganizerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizerHolder holder, int position) {
        holder.bindOrganizer(organizers.optJSONObject(position));
    }

    @Override
    public int getItemCount() {
        return organizers.length();
    }
}
