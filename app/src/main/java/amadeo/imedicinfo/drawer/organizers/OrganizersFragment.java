package amadeo.imedicinfo.drawer.organizers;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.Utils;

/**
 * Created by Amadeo on 2018-02-13.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class OrganizersFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organizers, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.organizersRecyclerView);

        try {
            String jsonString = Utils.loadInternalResourceAsString(requireActivity(), "organizers.conf");
            OrganizerAdapter adapter = new OrganizerAdapter(new JSONArray(jsonString));
            recyclerView.setAdapter(adapter);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);

            DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                    recyclerView.getContext(),
                    linearLayoutManager.getOrientation()
            );
            recyclerView.addItemDecoration(mDividerItemDecoration);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }
}
