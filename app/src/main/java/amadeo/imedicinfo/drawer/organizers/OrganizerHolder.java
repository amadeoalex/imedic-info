package amadeo.imedicinfo.drawer.organizers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import amadeo.imedicinfo.R;

/**
 * Created by Amadeo on 2018-02-13.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class OrganizerHolder extends RecyclerView.ViewHolder {

    private final TextView functionTextView;
    private final TextView nameTextView;
    private final TextView emailTextView;

    public OrganizerHolder(View itemView) {
        super(itemView);

        functionTextView = itemView.findViewById(R.id.organizerFunction);
        nameTextView = itemView.findViewById(R.id.organizerName);
        emailTextView = itemView.findViewById(R.id.organizerEmail);
    }

    public void bindOrganizer(JSONObject jsonObject) {
        functionTextView.setText(jsonObject.optString("function", "function"));
        nameTextView.setText(jsonObject.optString("name", "name"));

        String email = jsonObject.optString("email", "");

        if (!email.equals("")) {
            emailTextView.setText(email);
        } else {
            emailTextView.setVisibility(View.GONE);
        }

    }
}
