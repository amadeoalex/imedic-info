package amadeo.imedicinfo.drawer.settings;

import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import amadeo.imedicinfo.R;
import amadeo.imedicinfo.UpdateAsyncTask;

/**
 * Created by Amadeo on 2018-02-25.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);

        Preference update = findPreference("pref_key_update_now");
        update.setOnPreferenceClickListener(listener -> {
            UpdateAsyncTask updateAsyncTask = new UpdateAsyncTask(getActivity());
            updateAsyncTask.execute();

            return true;
        });
    }
}
