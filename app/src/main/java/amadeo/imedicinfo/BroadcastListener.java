package amadeo.imedicinfo;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import static amadeo.imedicinfo.NotificationBroadcastListener.CHANNEL_ID;

/**
 * Created by Amadeo on 2018-02-25.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */

public class BroadcastListener extends BroadcastReceiver {
    private static final String TAG = "iMedic Broadcast L.";
    private static final int MIN_UPDATE_DELAY = 86400000;

    private NetworkInfo previousNetworkInfo;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action != null) {

            switch (intent.getAction()) {
                case "android.intent.action.BOOT_COMPLETED":
                case "amadeo.imedic.action.SET_ALARM": {
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                    Intent pendingIntent = new Intent(context.getApplicationContext(), NotificationBroadcastListener.class);
                    pendingIntent.setAction("amadeo.imedic.action.NOTIFICATION");
                    PendingIntent alarmIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, pendingIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    if (alarmManager != null) {
                        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 5000, AlarmManager.INTERVAL_DAY, alarmIntent);
                        Utils.logI(TAG, "alarm set");
                    } else {
                        Utils.logW(TAG, "alarmManager is null!");
                    }

                    //copy paste, running out of time...
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_DEFAULT;
                        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, importance);
                        channel.setDescription(CHANNEL_ID);
                        // Register the channel with the system; you can't change the importance
                        // or other notification behaviors after this
                        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
                        if (notificationManager != null)
                            notificationManager.createNotificationChannel(channel);
                    }


                    break;
                }

                case "amadeo.imedic.action.REMOVE_ALARM": {
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                    Intent launchIntent = new Intent(context.getApplicationContext(), NotificationBroadcastListener.class);
                    launchIntent.setAction("amadeo.imedic.action.NOTIFICATION");
                    PendingIntent alarmIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, launchIntent, PendingIntent.FLAG_NO_CREATE);

                    if (alarmIntent == null)
                        Utils.logI(TAG, "alarm not removed - not set");
                    else {
                        if (alarmManager != null)
                            alarmManager.cancel(alarmIntent);
                        alarmIntent.cancel();
                        Utils.logI(TAG, "alarm canceled");
                    }

                    break;
                }

                case "amadeo.imedic.action.UPDATE":
                case "android.net.conn.CONNECTIVITY_CHANGE":
                    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (connectivityManager == null)
                        return;

                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();


                    if (networkInfo != null && networkInfo != previousNetworkInfo && networkInfo.isConnected()) {
                        previousNetworkInfo = networkInfo; //prevents receiving multiple broadcasts?

                        boolean isWiFi = networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
                        boolean isMobile = networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;

                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

                        long timeSinceLastUpdate = System.currentTimeMillis() - sharedPreferences.getLong("APP_LAST_UPDATE_MILLIS", 0);
                        if (timeSinceLastUpdate >= MIN_UPDATE_DELAY) {
                            Utils.logI(TAG, "scheduled update check");
                            if (isWiFi || (isMobile && sharedPreferences.getBoolean("pref_key_allow_update_over_mobile", true))) {
                                sharedPreferences.edit().putLong("APP_LAST_UPDATE_MILLIS", System.currentTimeMillis()).apply();

                                UpdateAsyncTask updateAsyncTask = new UpdateAsyncTask(context);
                                updateAsyncTask.execute();
                            }
                        } else
                            Utils.logI(TAG, "update canceled - %d < %d", timeSinceLastUpdate, MIN_UPDATE_DELAY);

                    } else {
                        Utils.logI(TAG, "network info is either null or connecting");
                    }
                    break;
            }

        } else {
            Utils.logI(TAG, "intent action is null");
        }
    }
}
