package amadeo.imedicinfo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import amadeo.imedicinfo.activity.MainActivity;

/**
 * Created by Amadeo on 2018-02-15.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */


public class UpdateAsyncTask extends AsyncTask<Void, Void, Integer> {
    private final String TAG = "iMedic UT";
    private final WeakReference<Context> contextWeakReference;
    private final WeakReference<UpdateCallback> callbackWeakReference;

    public interface UpdateCallback {
        void showSnackBar(String message);
    }

    public UpdateAsyncTask(Context context) {
        this.contextWeakReference = new WeakReference<>(context);

        if (context instanceof MainActivity) {
            this.callbackWeakReference = new WeakReference<>((UpdateCallback) context);
        } else {
            this.callbackWeakReference = null;
        }
    }

    @Override
    protected void onPreExecute() {
        Utils.logI(TAG, "checking for update");
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        JSONObject info = Utils.getURLResourceAsJSON(contextWeakReference.get().getResources().getString(R.string.wwwVersions));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(contextWeakReference.get());

        int status = 0;

        if (info != null) {
            Context context = contextWeakReference.get();

            double version = info.optDouble("version");
            double organizersVersion = info.optDouble("organizersVersion");
            double mapVersion = info.optDouble("mapVersion");
            double infoVersion = info.optDouble("infoVersion");
            double rulesVersion = info.optDouble("rulesVersion");

            if (version > sharedPreferences.getFloat("APP_DATA_VERSION", 0f)) { //I don't know why i even check this...
                Utils.logI(TAG, "update required");
                SharedPreferences.Editor editor = sharedPreferences.edit();

                if (organizersVersion > sharedPreferences.getFloat("APP_ORGANIZERS_VERSION", 0f)) {
                    try {
                        String jsonString = Utils.getURLResourceAsString(context.getResources().getString(R.string.wwwOrganizers));
                        File file = new File(context.getFilesDir().getAbsolutePath() + "/organizers.conf");
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        if (jsonString != null) {
                            fileOutputStream.write(jsonString.getBytes());
                        }
                        fileOutputStream.close();

                        editor.putFloat("APP_ORGANIZERS_VERSION", (float) organizersVersion);
                        status = 1;

                        Utils.logI(TAG, "organizers updated to version %f", organizersVersion);
                    } catch (FileNotFoundException e) {
                        status = -1;
                        Utils.logI(TAG, "organizers update failed - FNF");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (mapVersion > sharedPreferences.getFloat("APP_MAP_VERSION", 0f)) {
                    try {
                        HttpURLConnection urlConnection = (HttpURLConnection) new URL(context.getResources().getString(R.string.wwwMap)).openConnection();
                        unzipResources(urlConnection.getInputStream());
                        urlConnection.disconnect();


                        editor.putFloat("APP_MAP_VERSION", (float) mapVersion);
                        status = 1;

                        Utils.logI(TAG, "map updated to version %f", mapVersion);
                    } catch (FileNotFoundException e) {
                        status = -1;
                        Utils.logI(TAG, "map update failed - FNF");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (infoVersion > sharedPreferences.getFloat("APP_INFO_VERSION", 0f)) {
                    try {
                        HttpURLConnection urlConnection = (HttpURLConnection) new URL(context.getResources().getString(R.string.wwwInfo)).openConnection();
                        unzipResources(urlConnection.getInputStream());
                        urlConnection.disconnect();

                        editor.putFloat("APP_INFO_VERSION", (float) mapVersion);
                        status = 1;

                        Utils.logI(TAG, "info updated to version %f", infoVersion);
                    } catch (FileNotFoundException e) {
                        status = -1;
                        Utils.logI(TAG, "info update failed - FNF");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (rulesVersion > sharedPreferences.getFloat("APP_RULES_VERSION", 0f)) {
                    try {
                        String rules = Utils.getURLResourceAsString(context.getResources().getString(R.string.wwwRules));
                        File file = new File(context.getFilesDir().getAbsolutePath() + "/rules.conf");

                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        if (rules != null)
                            fileOutputStream.write(rules.getBytes());
                        fileOutputStream.close();

                        editor.putFloat("APP_RULES_VERSION", (float) rulesVersion);
                        status = 1;


                        Utils.logI(TAG, "rules updated to version %f", rulesVersion);
                    } catch (FileNotFoundException e) {
                        status = -1;
                        Utils.logI(TAG, "rules update failed - FNF");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


                editor.putFloat("APP_DATA_VERSION", (float) version);

                editor.apply();
            } else {
                Utils.logI(TAG, "files are up to date");
            }
        } else {
            status = -1;
            Utils.logI(TAG, "update failed");
            //cancel(true);
        }

        return status;
    }

    @Override
    protected void onPostExecute(Integer status) {
        Utils.logI(TAG, "update check finished");

        if (callbackWeakReference != null) {
            switch (status) {
                case 0:
                    callbackWeakReference.get().showSnackBar("Nothing to update.");
                    break;
                case 1:
                    callbackWeakReference.get().showSnackBar("Update finished!");
                    break;
                case -1:
                    callbackWeakReference.get().showSnackBar("Update error!");
                    break;
            }
        }
    }

    private void unzipResources(InputStream inputStream) {
        String outputDir = contextWeakReference.get().getFilesDir().getAbsolutePath();

        try {
            ZipInputStream zipInputStream = new ZipInputStream(inputStream);

            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                File file = new File(outputDir + "/" + zipEntry.getName());

                if (zipEntry.isDirectory()) {
                    if (!file.exists())
                        file.mkdirs();

                    continue;
                }

                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));

                byte buffer[] = new byte[1024];

                int count;
                while ((count = zipInputStream.read(buffer, 0, buffer.length)) != -1) {
                    bufferedOutputStream.write(buffer, 0, count);
                }

                bufferedOutputStream.flush();
                bufferedOutputStream.close();
            }

            zipInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
